use super::{GenerationOptions, Model, ModelMessage, ModelResult, ModelType, TextDecoder};
use crate::error::ThreadedModelError;
use async_channel::{Receiver, Sender};
use async_lock::Mutex;
use futures_lite::{future, stream::Boxed, Stream, StreamExt};
use std::{
    collections::HashMap,
    pin::Pin,
    sync::Arc,
    task::{Context, Poll},
    thread,
};

type ModelSender = Sender<(ModelMessage, Sender<ModelResult>)>;

#[derive(Clone)]
pub struct AsyncModel {
    tx: ModelSender,
    decoder: TextDecoder,
}

pub struct AsyncModelTokenGenerator {
    id: i32,
    tx: ModelSender,
    rx: Boxed<Option<String>>,
}

impl AsyncModel {
    pub async fn new(model_type: ModelType) -> Result<Self, ThreadedModelError> {
        log::debug!("Starting model thread");
        let (tx1, rx1) = async_channel::bounded(5);
        let (tx2, rx2) = async_channel::bounded(1);
        let _handle = thread::spawn(move || model_thread(model_type, rx1, tx2));
        let decoder = Self::wait_for_load(rx2).await?;

        Ok(Self { tx: tx1, decoder })
    }

    async fn wait_for_load(
        mut rx: Receiver<ModelResult>,
    ) -> Result<TextDecoder, ThreadedModelError> {
        log::debug!("Waiting for model load");
        let msg = {
            log_receive(
                rx.next()
                    .await
                    .ok_or(ThreadedModelError::MessageReceiveError)?,
            )
        };
        match msg {
            ModelResult::ModelLoaded(decoder) => {
                log::info!("Model loaded");
                Ok(decoder)
            }
            ModelResult::ModelError(e) => Err(e.into()),
            r => Err(ThreadedModelError::UnexpectedModelResult(r)),
        }
    }

    pub async fn create_token_generator(
        &self,
        text: String,
        generation_options: GenerationOptions,
    ) -> Result<AsyncModelTokenGenerator, ThreadedModelError> {
        log::debug!("Requested new generator");
        let msg = log_receive(
            self.send(log_send(ModelMessage::NewGenerator(
                text,
                generation_options,
            )))
            .await?,
        );
        match msg {
            ModelResult::StreamGenerator(id, rx) => {
                log::debug!("Created new generator with id {}", id);
                Ok(AsyncModelTokenGenerator::new(id, self.tx.clone(), rx))
            }
            r => Err(ThreadedModelError::UnexpectedModelResult(r)),
        }
    }

    pub fn get_decoder(&self) -> TextDecoder {
        self.decoder.clone()
    }

    pub fn decode_text(&self, tokens: Vec<i64>) -> String {
        self.decoder.decode_text(tokens)
    }

    pub fn get_token_id(&self, token: &str) -> i64 {
        self.decoder.get_token_id(token)
    }

    async fn send(&self, msg: ModelMessage) -> Result<ModelResult, ThreadedModelError> {
        send(msg, &self.tx).await
    }
}

impl AsyncModelTokenGenerator {
    fn new(id: i32, tx: ModelSender, rx: Receiver<ModelResult>) -> Self {
        let rx = rx
            .filter_map(|m| match m {
                ModelResult::Token(token) => Some(token),
                _ => None,
            })
            .boxed();
        Self { id, tx, rx }
    }

    pub async fn reset(&mut self) -> Result<(), ThreadedModelError> {
        let msg =
            log_receive(send(log_send(ModelMessage::ResetGenerator(self.id)), &self.tx).await?);
        match msg {
            ModelResult::Ok => Ok(()),
            r => Err(ThreadedModelError::UnexpectedModelResult(r)),
        }
    }
}

impl Drop for AsyncModelTokenGenerator {
    fn drop(&mut self) {
        let _ = future::block_on(send(ModelMessage::DropGenerator(self.id), &self.tx));
    }
}

impl Stream for AsyncModelTokenGenerator {
    type Item = String;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        match self.rx.as_mut().poll_next(cx) {
            Poll::Pending => Poll::Pending,
            Poll::Ready(item) => Poll::Ready(item.flatten()),
        }
    }
}

fn model_thread(
    model_type: ModelType,
    rx: Receiver<(ModelMessage, Sender<ModelResult>)>,
    tx: Sender<ModelResult>,
) -> Result<(), ThreadedModelError> {
    let exec = async_executor::LocalExecutor::new();
    let model = match Model::new(model_type) {
        Ok(model) => Arc::new(model),
        Err(err) => {
            future::block_on(tx.send(log_send(ModelResult::ModelError(err))))?;
            return Ok(());
        }
    };
    future::block_on(exec.run(async {
        let model = Arc::clone(&model);
        let decoder = model.get_decoder();
        tx.send(log_send(ModelResult::ModelLoaded(decoder))).await?;
        drop(tx);

        let mut generators = HashMap::new();
        let mut generator_id = 0;

        while let Ok((msg, tx)) = log_receive(rx.recv().await) {
            match msg {
                ModelMessage::NewGenerator(text, opts) => {
                    let (cmd_tx, cmd_rx) = async_channel::bounded(1);
                    let (result_tx, result_rx) = async_channel::bounded(3);
                    exec.spawn(run_generator(
                        Arc::clone(&model),
                        text,
                        opts,
                        result_tx,
                        result_rx.clone(),
                        cmd_rx,
                    ))
                    .detach();

                    generators.insert(generator_id, cmd_tx);
                    tx.send(log_send(ModelResult::StreamGenerator(
                        generator_id,
                        result_rx,
                    )))
                    .await?;
                    generator_id += 1;
                }
                ModelMessage::DropGenerator(id) => {
                    if let Some(cmd_tx) = generators.get(&id) {
                        cmd_tx.send(ModelMessage::DropGenerator(0)).await?;
                    }
                    generators.remove(&id);
                }
                ModelMessage::ResetGenerator(id) => {
                    if let Some(cmd_tx) = generators.get(&id) {
                        cmd_tx.send(ModelMessage::ResetGenerator(0)).await?;
                        tx.send(ModelResult::Ok).await?;
                    }
                }
                m => {
                    log::warn!("Received unexpected model message: {:?}", m);
                }
            }
        }

        Ok(())
    }))
}

async fn run_generator(
    model: Arc<Model>,
    text: String,
    generation_options: GenerationOptions,
    result_tx: Sender<ModelResult>,
    result_rx: Receiver<ModelResult>,
    cmd_rx: Receiver<ModelMessage>,
) {
    log::info!("Running detached generator task");
    let generator = model.create_token_generator_from_text(&text, generation_options);
    let generator = Mutex::new(generator);
    log::debug!("Generator created");
    let gen_future = future::or(
        async {
            loop {
                match log_receive(cmd_rx.recv().await?) {
                    ModelMessage::DropGenerator(_) => return Err(ThreadedModelError::EndTask),
                    ModelMessage::ResetGenerator(_) => {
                        log::trace!("Resetting async generator");
                        // don't scope the lock to prevent new tokens from being generated
                        let mut generator = generator.lock().await;
                        generator.reset();
                        let mut buf = Vec::new();
                        while let Ok(msg) = result_rx.try_recv() {
                            match msg {
                                ModelResult::Token(Some(token)) => {
                                    buf.push(token);
                                }
                                ModelResult::Token(None) => {
                                    log::trace!("Skipping generated None");
                                }
                                _ => {}
                            }
                            // yield to allow queued messages to be sent
                            future::yield_now().await;
                        }
                        for token in buf {
                            log::trace!("Resending already generated token");
                            let _ = result_tx
                                .send(log_send(ModelResult::Token(Some(token))))
                                .await;
                        }
                        log::trace!("Reset finished");
                    }
                    r => return Err(ThreadedModelError::UnexpectedModelMessage(r)),
                }
            }
            // only here as type annotation for the result type
            #[allow(unreachable_code)]
            Ok(())
        },
        async {
            loop {
                let token = {
                    let mut generator = generator.lock().await;
                    generator.next()
                };
                result_tx.send(log_send(ModelResult::Token(token))).await?;
            }
        },
    );
    let result: Result<(), _> = gen_future.await;
    if let Err(e) = result {
        match e {
            ThreadedModelError::EndTask => {}
            _ => log::error!("Error in generator task: {}", e),
        }
    }

    log::info!("Generator task ending");

    result_tx.close();
    cmd_rx.close();
}

async fn send(msg: ModelMessage, tx: &ModelSender) -> Result<ModelResult, ThreadedModelError> {
    let (tx2, mut rx) = async_channel::bounded(1);
    tx.send((msg, tx2)).await?;
    Ok(rx
        .next()
        .await
        .ok_or(ThreadedModelError::MessageReceiveError)?)
}

fn log_receive<T: std::fmt::Debug>(msg: T) -> T {
    log::trace!("received message: {:?}", msg);
    msg
}

fn log_send<T: std::fmt::Debug>(msg: T) -> T {
    log::trace!("sending message: {:?}", msg);
    msg
}

#[cfg(test)]
mod test {
    use super::*;

    #[async_std::test]
    async fn test_token_stream() -> Result<(), ThreadedModelError> {
        crate::set_model_path_to_cwd();
        let model = AsyncModel::new(ModelType::Gpt2Medium).await?;
        let mut generator = model
            .create_token_generator("hello world".into(), GenerationOptions::default())
            .await?;
        assert!(generator.next().await.is_some());
        Ok(())
    }
}
