use crate::model::{ModelMessage, ModelResult};
use thiserror::Error;

#[derive(Error, Debug)]
pub enum ThreadedModelError {
    #[error("Unexpected message from model thread: {0:?}")]
    UnexpectedModelResult(ModelResult),
    #[error("Unexpected message from model thread: {0:?}")]
    UnexpectedModelMessage(ModelMessage),
    #[error("Error loading model")]
    ModelLoadError(#[from] GptError),
    #[error("Model not loaded")]
    ModelNotLoaded,
    #[error("Couldn't receive message")]
    MessageReceiveError,
    #[error("Couldn't send message")]
    MessageSendError,
    #[cfg(feature = "async")]
    #[error("Task ending")]
    EndTask,
}

impl From<std::sync::mpsc::RecvError> for ThreadedModelError {
    fn from(_: std::sync::mpsc::RecvError) -> Self {
        Self::MessageReceiveError
    }
}

impl<T> From<std::sync::mpsc::SendError<T>> for ThreadedModelError {
    fn from(_: std::sync::mpsc::SendError<T>) -> Self {
        Self::MessageSendError
    }
}

#[cfg(feature = "async")]
impl<T> From<async_channel::SendError<T>> for ThreadedModelError {
    fn from(_: async_channel::SendError<T>) -> Self {
        Self::MessageSendError
    }
}

#[cfg(feature = "async")]
impl From<async_channel::RecvError> for ThreadedModelError {
    fn from(_: async_channel::RecvError) -> Self {
        Self::MessageReceiveError
    }
}

#[derive(Error, Debug)]
pub enum GptError {
    #[error("Failed to download model file \"{resource}\"")]
    FileDownloadError {
        resource: String,
        #[source]
        source: rust_bert::RustBertError,
    },
    #[error("Failed to create tokenizer")]
    TokenizerCreationError(#[from] rust_tokenizers::error::TokenizerError),
    #[error("Failed to load model")]
    ModelLoadError(#[from] tch::TchError),
    #[error("Failed to create pipeline")]
    PipelineError(#[source] rust_bert::RustBertError),
}

impl GptError {
    pub fn file_download_error(resource: &str, source: rust_bert::RustBertError) -> Self {
        Self::FileDownloadError {
            resource: String::from(resource),
            source,
        }
    }

    pub fn pipeline_error(source: rust_bert::RustBertError) -> Self {
        Self::PipelineError(source)
    }
}

#[derive(Error, Debug)]
pub enum GameError {
    #[error("A player with the name {0} already exists")]
    PlayerExists(String),
    #[error("A player with the name {0} does not exist")]
    UnknownPlayer(String),
}
