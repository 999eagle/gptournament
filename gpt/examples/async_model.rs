use futures_lite::StreamExt;
use gpt::model::{AsyncModel, GenerationOptions, ModelType};
use std::io::{self, Write};

#[async_std::main]
async fn main() {
    gpt::set_model_path_to_cwd();

    let text = "Hello world!";
    let model = AsyncModel::new(ModelType::Gpt2Large).await.unwrap();

    let options = GenerationOptions::default();

    let mut generator = model
        .create_token_generator(text.into(), options)
        .await
        .unwrap();
    print!("{}", text);
    while let Some(token) = generator.next().await {
        print!("{}", token);
        let _ = io::stdout().flush();
    }
}
