#[macro_export]
macro_rules! from_request {
    { $name: ident } => {
        impl actix_web::FromRequest for $name {
            type Error = GuardError;
            type Future = Ready<Result<Self, Self::Error>>;
            type Config = ();

            fn from_request(req: &HttpRequest, _payload: &mut Payload<PayloadStream>) -> Self::Future {
                ready(FromRequestSync::from_request_sync(req))
            }
        }
    }
}
