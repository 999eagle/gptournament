use crate::state::{model_state::StreamedState, ModelState, SingleState, StateId};
use futures_util::{future::LocalBoxFuture, lock::Mutex, stream::Stream, FutureExt, StreamExt};
use gpt::{game::AsyncGame, model::AsyncModelTokenGenerator, ThreadedModelError};
use std::{
    pin::Pin,
    sync::Arc,
    task::{Context, Poll},
};

pub struct StreamedStateGenerator<T: Stream<Item = String>> {
    state: Arc<Mutex<SingleState<StreamedState<T>>>>,
    future: Option<LocalBoxFuture<'static, Option<String>>>,
}

impl<T: Stream<Item = String> + Unpin + ResetGenerator> StreamedStateGenerator<T> {
    pub async fn from_model_states(
        model_states: &ModelState<StreamedState<T>>,
        id: StateId,
    ) -> Result<Self, anyhow::Error> {
        let state = if let Some(state) = model_states.get_inner(id).await {
            state
        } else {
            anyhow::bail!("wrong id");
        };
        {
            let mut state = state.lock().await;
            let _ = state.state_mut().reset().await;
        }
        Ok(Self::new(state))
    }
}

impl<T: Stream<Item = String> + Unpin> StreamedStateGenerator<T> {
    pub fn new(state: Arc<Mutex<SingleState<StreamedState<T>>>>) -> Self {
        Self {
            state,
            future: None,
        }
    }

    async fn next_token(state: Arc<Mutex<SingleState<StreamedState<T>>>>) -> Option<String> {
        let mut state = state.lock().await;
        let stream_state = state.state_mut();
        let token = stream_state.next().await;
        if let Some(token) = &token {
            state.send_to_websockets(token.clone()).await;
        }
        token
    }
}

impl<T: Stream<Item = String> + Unpin + ResetGenerator + 'static> Stream
    for StreamedStateGenerator<T>
{
    type Item = String;

    fn poll_next(mut self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let mut future = match self.future.take() {
            Some(future) => future,
            None => {
                let game = Arc::clone(&self.state);
                Self::next_token(game).boxed_local()
            }
        };
        match future.poll_unpin(cx) {
            Poll::Ready(item) => Poll::Ready(item),
            Poll::Pending => {
                self.future = Some(future);
                Poll::Pending
            }
        }
    }
}

#[async_trait::async_trait]
pub trait ResetGenerator {
    async fn reset(&mut self) -> Result<(), ThreadedModelError>;
}

#[async_trait::async_trait]
impl ResetGenerator for AsyncGame {
    async fn reset(&mut self) -> Result<(), ThreadedModelError> {
        self.reset_generator().await
    }
}

#[async_trait::async_trait]
impl ResetGenerator for AsyncModelTokenGenerator {
    async fn reset(&mut self) -> Result<(), ThreadedModelError> {
        self.reset().await
    }
}
