use crate::state::StateId;
use serde::Deserialize;

#[derive(Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "snake_case")]
pub struct UserLogin {
    pub(crate) username: String,
    pub(crate) password: String,
}

#[derive(Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "snake_case")]
pub struct Token {
    pub(crate) token: String,
}

#[derive(Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "snake_case")]
pub struct Id {
    pub(crate) id: StateId,
}

#[derive(Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "snake_case")]
pub struct IdLine {
    pub(crate) id: StateId,
    pub(crate) line: String,
}

#[derive(Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "snake_case")]
pub struct Line {
    pub(crate) line: String,
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_de_user_login() {
        assert_eq!(
            serde_urlencoded::from_str::<UserLogin>(
                "username=testuser&password=verysecurepassword"
            )
            .unwrap(),
            UserLogin {
                username: "testuser".into(),
                password: "verysecurepassword".into()
            }
        );
    }

    #[test]
    fn test_de_token() {
        assert_eq!(
            serde_urlencoded::from_str::<Token>("token=token_value").unwrap(),
            Token {
                token: "token_value".into()
            }
        );
    }

    #[test]
    fn test_de_id() {
        assert_eq!(
            serde_urlencoded::from_str::<Id>("id=0").unwrap(),
            Id { id: 0 }
        );
    }

    #[test]
    fn test_de_id_line() {
        assert_eq!(
            serde_urlencoded::from_str::<IdLine>("id=0&line=Hello,%20world!%0a").unwrap(),
            IdLine {
                id: 0,
                line: "Hello, world!\n".into()
            }
        );
    }

    #[test]
    fn test_de_line() {
        assert_eq!(
            serde_urlencoded::from_str::<Line>("line=Hello,%20world!%0a").unwrap(),
            Line {
                line: "Hello, world!\n".into()
            }
        );
    }
}
