use crate::state::StateId;
use actix::Message;
use serde::Deserialize;

pub enum ServerMessage {
    Text(String),
    Close,
}

#[derive(Deserialize, Debug, Clone, Eq, PartialEq)]
#[serde(rename_all = "snake_case")]
pub enum ClientMessage {
    AuthToken(String),
    UserCookie(String),
    Game(StateId),
    Generator(StateId),
}

impl Message for ServerMessage {
    type Result = ();
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_de_client_message_auth_token() {
        assert_eq!(
            serde_json::from_str::<ClientMessage>("{\"auth_token\":\"token_value\"}").unwrap(),
            ClientMessage::AuthToken("token_value".into())
        );
    }

    #[test]
    fn test_de_client_message_user_cookie() {
        assert_eq!(
            serde_json::from_str::<ClientMessage>("{\"user_cookie\":\"cookie\"}").unwrap(),
            ClientMessage::UserCookie("cookie".into())
        );
    }

    #[test]
    fn test_de_client_message_game() {
        assert_eq!(
            serde_json::from_str::<ClientMessage>("{\"game\":0}").unwrap(),
            ClientMessage::Game(0)
        );
    }

    #[test]
    fn test_de_client_message_generator() {
        assert_eq!(
            serde_json::from_str::<ClientMessage>("{\"generator\":0}").unwrap(),
            ClientMessage::Generator(0)
        );
    }
}
