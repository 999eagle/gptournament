pub mod request;
pub mod response;
pub mod websocket;

pub use request::*;
pub use response::*;
pub use websocket::*;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct UserData {
    pub(crate) username: String,
    pub(crate) client_ip: String,
}
