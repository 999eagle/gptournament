use super::helper::{
    generator::{ResetGenerator, StreamedStateGenerator},
    guards::Authentication,
    types::{request, IdResponse, Response, StatusResponse},
};
use crate::{
    config::Config,
    resources::Model,
    state::{model_state::StreamedState, Games, Generators, ModelState, StateId},
    websocket::Connection,
};
use actix_web::web::Bytes;
use actix_web::{web, Either, HttpRequest, HttpResponse};
use actix_web_actors::ws;
use futures_util::stream::Stream;
use futures_util::StreamExt;
use gpt::game::AsyncGame;

pub async fn create_generator(
    line: String,
    model: web::Data<Model>,
    generators: web::Data<Generators>,
    config: web::Data<Config>,
) -> IdResponse {
    log::debug!("Creating generator from input {:?}", line);
    let line = filter_input(&line);
    let generator = match model
        .create_token_generator(line, config.generation_options)
        .await
    {
        Ok(generator) => generator,
        Err(e) => {
            log::error!("Failed to create a generator: {}", e);
            return Response::error("failed to create a generator");
        }
    };
    let id = generators.add_state(generator);
    Response::ok(id)
}

pub async fn create_generator_get(
    _auth: Authentication,
    web::Query(q): web::Query<request::Line>,
    model: web::Data<Model>,
    generators: web::Data<Generators>,
    config: web::Data<Config>,
) -> IdResponse {
    let request::Line { line } = q;
    create_generator(line, model, generators, config).await
}

pub async fn create_generator_post(
    _auth: Authentication,
    line: String,
    model: web::Data<Model>,
    generators: web::Data<Generators>,
    config: web::Data<Config>,
) -> IdResponse {
    create_generator(line, model, generators, config).await
}

pub async fn create_game(
    _auth: Authentication,
    model: web::Data<Model>,
    games: web::Data<Games>,
    config: web::Data<Config>,
) -> IdResponse {
    let game = match AsyncGame::new(&model, config.generation_options).await {
        Ok(game) => game,
        Err(e) => {
            log::error!("Failed to create a game: {}", e);
            return Response::error("failed to create a game");
        }
    };
    let id = games.add_state(game);
    Response::ok(id)
}

pub async fn add_input(id: StateId, line: String, games: web::Data<Games>) -> StatusResponse {
    log::debug!("Adding input to game {}: {:?}", id, line);
    let line = filter_input(&line);
    let game = if let Some(game) = games.get_inner(id).await {
        game
    } else {
        return StatusResponse::error("wrong game id");
    };
    let result = {
        let mut game = game.lock().await;
        game.state_mut().add_input_line(&line).await
    };
    match result {
        Ok(_) => StatusResponse::Ok,
        Err(e) => StatusResponse::error(format!("{:?}", e)),
    }
}

pub async fn add_input_get(
    _auth: Authentication,
    web::Query(q): web::Query<request::IdLine>,
    games: web::Data<Games>,
) -> StatusResponse {
    let request::IdLine { id, line } = q;
    add_input(id, line, games).await
}

pub async fn add_input_post(
    _auth: Authentication,
    web::Query(q): web::Query<request::Id>,
    line: String,
    games: web::Data<Games>,
) -> StatusResponse {
    add_input(q.id, line, games).await
}

pub async fn generate<T: Stream<Item = String> + Unpin + ResetGenerator + 'static>(
    _auth: Authentication,
    web::Query(id): web::Query<request::Id>,
    model_states: web::Data<ModelState<StreamedState<T>>>,
) -> Either<HttpResponse, StatusResponse> {
    let id = id.id;
    StreamedStateGenerator::from_model_states(&model_states, id)
        .await
        .map_or_else(
            |_| Either::B(StatusResponse::error("wrong id")),
            |stream| {
                let stream = stream
                    .inspect(|t| {
                        log::trace!("Sending GPT output token: {:?}", t);
                    })
                    .map(|t| Bytes::copy_from_slice(t.as_bytes()))
                    .map(Result::<_, ()>::Ok);
                Either::A(HttpResponse::Ok().streaming(stream))
            },
        )
}

pub async fn stream(
    auth: Option<Authentication>,
    req: HttpRequest,
    stream: web::Payload,
) -> Either<HttpResponse, StatusResponse> {
    let mut conn = match Connection::new(&req) {
        Some(conn) => conn,
        None => {
            return Either::B(StatusResponse::error(
                "internal error creating web socket connection",
            ))
        }
    };
    if auth.is_some() {
        conn.set_authenticated();
    }
    let resp = ws::start(conn, &req, stream);
    match resp {
        Ok(resp) => {
            log::info!(
                "Incoming web socket connection from {:?} started",
                req.connection_info().realip_remote_addr()
            );
            Either::A(resp)
        }
        Err(e) => {
            log::error!("Error starting web socket connection: {}", e);
            Either::B(StatusResponse::error(
                "internal error creating web socket connection",
            ))
        }
    }
}

fn filter_input(input: &str) -> String {
    let line = input
        .replace('\r', "\n")
        .chars()
        .fold((String::new(), false), |(mut text, is_newline), b| {
            if !is_newline || b != '\n' {
                text.push(b)
            }
            (text, b == '\n')
        })
        .0;
    log::trace!("Filtered input {:?} to {:?}", input, line);
    line
}

#[cfg(test)]
mod test {
    use super::filter_input;

    #[test]
    fn test_filter() {
        assert_eq!(filter_input("Test\r\ntest"), "Test\ntest");
        assert_eq!(filter_input("Test\rtest"), "Test\ntest");
        assert_eq!(filter_input("Test\r\ntest\r\r\r\n"), "Test\ntest\n");
    }
}
