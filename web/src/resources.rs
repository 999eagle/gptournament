use crate::state::{model_state::ModelStateImpl, ModelState};
use gpt::model::AsyncModel;
use std::ops::Deref;
use std::sync::{Arc, Weak};
use std::time::Duration;
use tokio::task::JoinHandle;

#[derive(Clone)]
pub struct Model {
    model: Arc<AsyncModel>,
}

impl Model {
    pub fn new(model: AsyncModel) -> Self {
        Self {
            model: Arc::new(model),
        }
    }
}

impl Deref for Model {
    type Target = AsyncModel;

    fn deref(&self) -> &Self::Target {
        &*self.model
    }
}

pub struct StateCleanup {
    handles: Vec<JoinHandle<()>>,
}

impl Default for StateCleanup {
    fn default() -> Self {
        Self::new()
    }
}

impl StateCleanup {
    pub fn new() -> Self {
        Self {
            handles: Vec::new(),
        }
    }

    pub fn add_state<T: 'static + Send>(&mut self, state: ModelState<T>) {
        let weak = state.get_weak();
        self.handles
            .push(tokio::task::spawn(Self::state_cleanup_task(weak)));
    }

    async fn state_cleanup_task<T>(states: Weak<ModelStateImpl<T>>) {
        log::info!("Starting cleanup thread");
        let mut interval = tokio::time::interval(Duration::from_secs(60));
        loop {
            interval.tick().await;
            if let Some(states) = states.upgrade() {
                log::debug!("Running cleanup");
                states.cleanup().await;
            } else {
                break;
            }
        }
    }
}
