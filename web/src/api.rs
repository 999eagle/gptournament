mod gpt;
pub mod helper;
mod user;

use ::gpt::{game::AsyncGame, model::AsyncModelTokenGenerator};
use actix_web::web;

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(web::resource("/game/create").route(web::get().to(gpt::create_game)))
        .service(
            web::resource("/game/add_input")
                .route(web::get().to(gpt::add_input_get))
                .route(web::post().to(gpt::add_input_post)),
        )
        .service(web::resource("/game/generate").route(web::get().to(gpt::generate::<AsyncGame>)))
        .service(
            web::resource("/generator/create")
                .route(web::get().to(gpt::create_generator_get))
                .route(web::post().to(gpt::create_generator_post)),
        )
        .service(
            web::resource("/generator/generate")
                .route(web::get().to(gpt::generate::<AsyncModelTokenGenerator>)),
        )
        .service(web::resource("/stream").route(web::get().to(gpt::stream)))
        .service(web::resource("/login").route(web::post().to(user::login)))
        .service(
            web::resource("/add_user")
                .route(web::get().to(user::add_user))
                .route(web::post().to(user::add_user_token)),
        )
        .service(web::resource("/list_users").route(web::get().to(user::list_users)));
}
