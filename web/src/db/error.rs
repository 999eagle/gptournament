use std::error::Error;
use std::fmt;

#[derive(Debug)]
pub enum DBError {
    R2D2Error(r2d2::Error),
    DieselError(diesel::result::Error),
    UniqueViolation(Box<dyn diesel::result::DatabaseErrorInformation + Send + Sync>),
    NoResults,
}

impl fmt::Display for DBError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::NoResults => write!(f, "No results found"),
            Self::DieselError(error) => write!(f, "Error from DB driver: {}", error),
            Self::R2D2Error(error) => write!(f, "Error from DB pool: {}", error),
            Self::UniqueViolation(error) => match (error.table_name(), error.column_name()) {
                (Some(table), Some(column)) => {
                    write!(f, "Unique constraint violated on `{}`.`{}`", table, column,)
                }
                _ => write!(f, "Unique constraint violated: {}", error.message()),
            },
        }
    }
}

impl Error for DBError {}

impl From<diesel::result::Error> for DBError {
    fn from(error: diesel::result::Error) -> Self {
        match error {
            diesel::result::Error::DatabaseError(
                diesel::result::DatabaseErrorKind::UniqueViolation,
                info,
            ) => Self::UniqueViolation(info),
            e => Self::DieselError(e),
        }
    }
}

impl From<r2d2::Error> for DBError {
    fn from(e: r2d2::Error) -> Self {
        Self::R2D2Error(e)
    }
}
