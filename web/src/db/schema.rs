table! {
    users {
        id -> Integer,
        name -> VarChar,
        password -> VarChar,
        is_admin -> Bool,
    }
}

table! {
    tokens {
        id -> Integer,
        token -> VarChar,
        creating_user -> VarChar,
        expiry -> Timestamp,
    }
}
