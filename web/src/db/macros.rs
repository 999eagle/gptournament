#[macro_export(local_inner_macros)]
macro_rules! table_struct {
	{
		$name: ident, $newname: ident -> $tablestr: tt, $table: ident { $( $field: ident : $field_type: ty = $default: expr, )* }
		fn get($($get_field: ident : $get_field_type: ty),*);
	} => {
		#[derive(Queryable, Identifiable, AsChangeset, Clone, Debug)]
		pub struct $name {
			id: i32,
			$(
				pub $field: $field_type,
			)*
		}

		#[derive(Insertable, Clone, Debug)]
		#[table_name = $tablestr]
		pub struct $newname<'a> {
			$(
				$field: &'a $field_type,
			)*
		}

		impl Default for $name {
			fn default() -> Self {
				Self::new()
			}
		}

		impl $name {
			pub fn new() -> Self {
				Self::new_from_values($($default),*)
			}

			pub fn new_from_values($($field: $field_type),*) -> Self {
				Self {
					id: 0,
					$(
						$field,
					)*
				}
			}

			pub fn get(conn: &diesel::SqliteConnection, $($get_field: $get_field_type),*) -> Result<Self, DBError> {
		        let mut values = $table::table
		            $(
		                .filter($table::$get_field.eq($get_field))
		            )*
		            .load::<$name>(conn)?;
		        match values.len() {
		            1 => Ok(values.pop().unwrap()),
		            _ => Err(DBError::NoResults),
		        }
			}

			pub fn get_all(conn: &diesel::SqliteConnection) -> Result<Vec<Self>, DBError> {
			    Ok($table::table.load::<$name>(conn)?)
			}
		}

		impl<'a> Model<'a> for $name {
			type Insert = $newname<'a>;

            fn update(&self, conn: &diesel::SqliteConnection) -> Result<(), DBError> {
                diesel::update(self)
                    .set(self)
                    .execute(conn)?;
                Ok(())
            }

	        fn add(conn: &diesel::SqliteConnection, value: Self::Insert) -> Result<(), DBError> {
	            diesel::insert_into($table::table)
	                .values(value)
	                .execute(conn)?;
	            Ok(())
	        }

			fn as_insert(&'a self) -> Self::Insert {
				Self::Insert {
					$(
						$field: &self.$field,
					)*
				}
			}
		}
	}
}
