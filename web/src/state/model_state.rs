use crate::api::helper::types::{response, websocket::ServerMessage};
use futures_util::{lock::Mutex, SinkExt, StreamExt};
use gpt::{game::AsyncGame, model::AsyncModelTokenGenerator};
use std::collections::HashMap;
use std::ops::{Deref, DerefMut};
use std::sync::atomic::{AtomicU32, Ordering};
use std::sync::{Arc, RwLock, Weak};
use std::time::{Duration, Instant};

pub type Game = StreamedState<AsyncGame>;
pub type Generator = StreamedState<AsyncModelTokenGenerator>;
pub type Games = ModelState<Game>;
pub type Generators = ModelState<Generator>;

pub type StateId = u32;

pub struct ModelState<T> {
    inner: Arc<ModelStateImpl<T>>,
}

impl<T> Default for ModelState<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> Clone for ModelState<T> {
    fn clone(&self) -> Self {
        Self {
            inner: Arc::clone(&self.inner),
        }
    }
}

impl<T> ModelState<T> {
    pub fn new() -> Self {
        Self {
            inner: Arc::new(ModelStateImpl::new()),
        }
    }

    pub fn clone(&self) -> Self {
        Self {
            inner: Arc::clone(&self.inner),
        }
    }

    pub fn get_weak(&self) -> Weak<ModelStateImpl<T>> {
        Arc::downgrade(&self.inner)
    }
}

impl<T> Deref for ModelState<T> {
    type Target = ModelStateImpl<T>;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

pub struct ModelStateImpl<T> {
    states: RwLock<HashMap<StateId, Arc<Mutex<SingleState<T>>>>>,
    current_id: AtomicU32,
}

impl<T> Default for ModelStateImpl<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T> ModelStateImpl<T> {
    pub fn new() -> Self {
        Self {
            states: RwLock::new(HashMap::new()),
            current_id: AtomicU32::new(0),
        }
    }

    pub async fn get_inner(&self, id: StateId) -> Option<Arc<Mutex<SingleState<T>>>> {
        let arc = {
            let states = self.states.read().unwrap();
            states.get(&id).map(Arc::clone)
        };
        if let Some(arc) = arc {
            {
                let mut lock = arc.lock().await;
                lock.last_use = Instant::now();
            }
            Some(arc)
        } else {
            None
        }
    }

    pub fn add_state<U: Into<T>>(&self, state: U) -> StateId {
        let mut states = self.states.write().unwrap();
        let id = self.current_id.fetch_add(1, Ordering::SeqCst);
        states.insert(
            id,
            Arc::new(Mutex::new(SingleState {
                inner: state.into(),
                last_use: Instant::now(),
            })),
        );
        id
    }

    pub async fn cleanup(&self) {
        let mut cleanup = Vec::new();
        {
            let states = self.states.read().unwrap();
            for (id, state) in states.iter() {
                cleanup.push((*id, Arc::clone(state)));
            }
        }
        let cleanup = futures_util::stream::iter(cleanup)
            .filter_map(|(id, state)| async move {
                let state = state.lock().await;
                if Instant::now().duration_since(state.last_use) > Duration::from_secs(60 * 10) {
                    Some(id)
                } else {
                    None
                }
            })
            .collect::<Vec<_>>()
            .await;
        if !cleanup.is_empty() {
            log::info!("Cleaning up {} states", cleanup.len());
            let mut states = self.states.write().unwrap();
            for id in cleanup {
                states.remove(&id);
                log::debug!(
                    "Cleaned up state {} with id {}",
                    std::any::type_name::<T>(),
                    id
                );
            }
        }
    }
}

pub struct SingleState<T> {
    inner: T,
    last_use: Instant,
}

impl<T> Deref for SingleState<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.inner
    }
}

impl<T> DerefMut for SingleState<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner
    }
}

pub struct StreamedState<T> {
    inner: T,
    current_id: AtomicU32,
    ws_connections: HashMap<u32, Arc<Mutex<crate::websocket::GameConnection>>>,
}

impl<T> StreamedState<T> {
    pub fn new(state: T) -> Self {
        Self {
            inner: state,
            current_id: AtomicU32::new(0),
            ws_connections: HashMap::new(),
        }
    }

    pub fn state_mut(&mut self) -> &mut T {
        &mut self.inner
    }

    pub fn add_ws_connection(&mut self, connection: crate::websocket::GameConnection) -> u32 {
        let id = self.current_id.fetch_add(1, Ordering::SeqCst);
        self.ws_connections
            .insert(id, Arc::new(Mutex::new(connection)));
        id
    }

    pub fn remove_ws_connection(&mut self, connection_id: u32) {
        self.ws_connections.remove(&connection_id);
    }

    pub async fn send_to_websockets(&self, token: String) {
        let token = response::TokenResponse::ok(token);
        let token = serde_json::to_string(&token).unwrap();
        let handles = self
            .ws_connections
            .iter()
            .map(|(_, conn)| {
                let conn = Arc::clone(conn);
                let token = token.clone();
                tokio::task::spawn(async move {
                    let mut conn = conn.lock().await;
                    let _ = conn.tx.send(ServerMessage::Text(token)).await;
                })
            })
            .collect::<Vec<_>>();
        for h in handles {
            let _ = h.await;
        }
    }

    pub async fn close_connections(&mut self) {
        log::debug!("Closing all remaining web socket connections");
        for (_, conn) in self.ws_connections.drain() {
            let mut conn = conn.lock().await;
            let _ = conn.tx.send(ServerMessage::Close).await;
        }
    }
}

impl<T> From<T> for StreamedState<T> {
    fn from(state: T) -> Self {
        Self::new(state)
    }
}

impl<T> Drop for StreamedState<T> {
    fn drop(&mut self) {
        futures_executor::block_on(async move { self.close_connections().await });
    }
}
