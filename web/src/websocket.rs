use crate::{
    api::helper::{
        cookie::CookieBuilder,
        guards::{ApiKey, ClientIp, FromRequestSync, User},
        types::{
            response,
            websocket::{ClientMessage, ServerMessage},
            UserData,
        },
    },
    config::Config,
    db::DbConn,
    state::{model_state::StreamedState, Games, Generators, ModelState, StateId},
};
use actix::{Actor, ActorContext, ActorFuture, AsyncContext, Handler, Running, StreamHandler};
use actix_web::{web, HttpRequest};
use actix_web_actors::ws::{self, Message, ProtocolError, WebsocketContext};
use cookie::{Cookie, CookieJar};
use futures_channel::mpsc::{self, Sender};
use futures_executor::block_on;

pub struct GameConnection {
    pub(crate) tx: Sender<ServerMessage>,
}

pub struct Connection {
    config: Config,
    games: Games,
    generators: Generators,
    cookie_builder: CookieBuilder,
    client_ip: ClientIp,
    db: DbConn,
    is_authenticated: bool,
    handle: Option<ConnectionHandle>,
}

enum ConnectionHandle {
    Game(StateId, u32),
    Generator(StateId, u32),
}

impl Connection {
    pub fn new(req: &HttpRequest) -> Option<Self> {
        log::debug!("Creating new web socket connection");
        let config = req.app_data::<web::Data<Config>>()?.get_ref().clone();
        let games = req.app_data::<web::Data<Games>>()?.get_ref().clone();
        let generators = req.app_data::<web::Data<Generators>>()?.get_ref().clone();
        let cookie_builder = req
            .app_data::<web::Data<CookieBuilder>>()?
            .get_ref()
            .clone();
        let client_ip = ClientIp::from_request_sync(req).ok()?;
        let db = DbConn::from_request_sync(req).ok()?;
        Some(Self {
            config,
            games,
            generators,
            cookie_builder,
            client_ip,
            db,
            is_authenticated: false,
            handle: None,
        })
    }

    pub fn set_authenticated(&mut self) {
        self.is_authenticated = true;
    }

    async fn stop<T>(states: &ModelState<StreamedState<T>>, state_id: StateId, conn_id: u32) {
        let state = states.get_inner(state_id).await;
        if let Some(state) = state {
            log::debug!("Removing web socket connection from state");
            let mut state = state.lock().await;
            state.remove_ws_connection(conn_id);
        }
    }

    fn attach<T: 'static, F: Fn(StateId, u32) -> ConnectionHandle + 'static>(
        ctx: &mut <Self as Actor>::Context,
        states: &ModelState<StreamedState<T>>,
        state_id: StateId,
        handle: F,
    ) {
        let states = states.clone();
        ctx.wait(
            actix::fut::wrap_future(async move {
                if let Some(state) = states.get_inner(state_id).await {
                    let mut state = state.lock().await;
                    let (tx, rx) = mpsc::channel(1);
                    let conn_id = state.add_ws_connection(GameConnection { tx });
                    Some((rx, state_id, conn_id))
                } else {
                    None
                }
            })
            .map(
                move |result, actor: &mut Self, ctx: &mut WebsocketContext<Self>| {
                    if let Some((rx, state_id, conn_id)) = result {
                        ctx.add_message_stream(rx);
                        let handle = handle(state_id, conn_id);
                        actor.handle = Some(handle);
                        let msg = serde_json::to_string(&response::StatusResponse::Ok).unwrap();
                        ctx.notify(ServerMessage::Text(msg));
                    } else {
                        let msg =
                            serde_json::to_string(&response::StatusResponse::error("wrong id"))
                                .unwrap();
                        ctx.notify(ServerMessage::Text(msg));
                    }
                },
            ),
        );
    }
}

impl Actor for Connection {
    type Context = ws::WebsocketContext<Self>;

    fn stopping(&mut self, _ctx: &mut Self::Context) -> Running {
        log::trace!("Web socket actor stopping");
        match self.handle.take() {
            Some(ConnectionHandle::Game(game_id, conn_id)) => {
                block_on(Self::stop(&self.games, game_id, conn_id))
            }
            Some(ConnectionHandle::Generator(generator_id, conn_id)) => {
                block_on(Self::stop(&self.generators, generator_id, conn_id))
            }
            _ => {}
        }
        log::info!("Web socket connection stopped");
        Running::Stop
    }
}

impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for Connection {
    fn handle(&mut self, item: Result<Message, ProtocolError>, ctx: &mut Self::Context) {
        log::debug!("Handling incoming web socket message: {:?}", item);
        match item {
            Ok(Message::Text(msg)) => match serde_json::from_str(&msg) {
                Err(e) => log::warn!("Received invalid web socket message from client: {}", e),
                Ok(ClientMessage::AuthToken(token)) if !self.is_authenticated => {
                    if ApiKey::key_is_valid(&self.config, &token) {
                        self.is_authenticated = true;
                        let msg = serde_json::to_string(&response::StatusResponse::Ok).unwrap();
                        ctx.notify(ServerMessage::Text(msg));
                    } else {
                        let msg = serde_json::to_string(&response::StatusResponse::error(
                            "unauthenticated",
                        ))
                        .unwrap();
                        ctx.notify(ServerMessage::Text(msg));
                    }
                }
                Ok(ClientMessage::UserCookie(cookie)) if !self.is_authenticated => {
                    let mut jar = CookieJar::new();
                    jar.add_original(Cookie::new("user", cookie));
                    let cookies = self.cookie_builder.new_from_jar(jar);
                    let user = cookies
                        .get_secret("user")
                        .map(|cookie| serde_json::from_str::<UserData>(cookie.value()).ok())
                        .flatten()
                        .map(|user_data| {
                            User::try_from_user_data(user_data, &self.client_ip, &self.db).ok()
                        })
                        .flatten();
                    if user.is_some() {
                        self.is_authenticated = true;
                        let msg = serde_json::to_string(&response::StatusResponse::Ok).unwrap();
                        ctx.notify(ServerMessage::Text(msg));
                    } else {
                        let msg = serde_json::to_string(&response::StatusResponse::error(
                            "unauthenticated",
                        ))
                        .unwrap();
                        ctx.notify(ServerMessage::Text(msg));
                    }
                }
                Ok(ClientMessage::AuthToken(_)) => {}
                Ok(ClientMessage::UserCookie(_)) => {}
                Ok(_) if !self.is_authenticated => {
                    let msg =
                        serde_json::to_string(&response::StatusResponse::error("unauthenticated"))
                            .unwrap();
                    ctx.notify(ServerMessage::Text(msg));
                }
                Ok(ClientMessage::Game(game_id)) => {
                    Self::attach(ctx, &self.games, game_id, ConnectionHandle::Game);
                }
                Ok(ClientMessage::Generator(generator_id)) => {
                    Self::attach(
                        ctx,
                        &self.generators,
                        generator_id,
                        ConnectionHandle::Generator,
                    );
                }
            },
            Ok(Message::Close(reason)) => {
                log::info!("Web socket connection closed by client: {:?}", reason);
            }
            Err(e) => {
                log::error!("Protocol error on web socket: {}", e);
            }
            _ => (),
        }
    }
}

impl Handler<ServerMessage> for Connection {
    type Result = ();

    fn handle(&mut self, item: ServerMessage, ctx: &mut Self::Context) -> Self::Result {
        match item {
            ServerMessage::Text(text) => ctx.text(text),
            ServerMessage::Close => {
                ctx.close(None);
                ctx.stop();
            }
        }
    }
}
