use crate::api::helper::types;
use actix_web::{http::StatusCode, HttpResponse, ResponseError};
use thiserror::Error;

#[derive(Debug, Clone, Error)]
pub enum GuardError {
    #[error("unauthenticated")]
    Unauthenticated,
    #[error("internal server error")]
    InternalServerError,
    #[error("{0}")]
    General(String),
}

impl ResponseError for GuardError {
    fn status_code(&self) -> StatusCode {
        match self {
            GuardError::Unauthenticated => StatusCode::FORBIDDEN,
            GuardError::InternalServerError => StatusCode::INTERNAL_SERVER_ERROR,
            _ => StatusCode::OK,
        }
    }

    fn error_response(&self) -> HttpResponse {
        HttpResponse::build(self.status_code()).json(types::StatusResponse::error(self.to_string()))
    }
}

impl From<anyhow::Error> for GuardError {
    fn from(e: anyhow::Error) -> Self {
        Self::General(e.to_string())
    }
}
