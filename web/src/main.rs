#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

mod api;
mod config;
mod db;
mod error;
mod middleware;
mod resources;
mod state;
mod websocket;

use actix_web::{middleware as actix_middleware, web};
use clap::{App, Arg};
use gpt::model::AsyncModel;
use resources::Model;
use std::path::{Path, PathBuf};
use version::version;

fn main() {
    let mut rt = tokio::runtime::Builder::new()
        .threaded_scheduler()
        .enable_time()
        .enable_io()
        .build()
        .expect("runtime");
    let result = rt.block_on(main_result());
    if let Err(e) = result {
        log::error!("{}", e);
    }
}

async fn main_result() -> Result<(), anyhow::Error> {
    let crate_setup = bin_common::CrateSetupBuilder::new()
        .with_app_name("gptournament-web")
        .build()
        .expect("CrateSetup");
    let matches = App::new(crate_setup.application_name())
        .version(version!())
        .arg(
            Arg::with_name("v")
                .short("v")
                .long("verbose")
                .multiple(true)
                .help("Increase verbosity. May be specified multiple times."),
        )
        .arg(
            Arg::with_name("model")
                .long("model")
                .takes_value(true)
                .value_name("MODEL PATH")
                .help("Path to model."),
        )
        .arg(
            Arg::with_name("config")
                .long("config")
                .short("c")
                .takes_value(true)
                .value_name("CONFIG_PATH")
                .help("Config file path"),
        )
        .get_matches();
    let verbosity = matches.occurrences_of("v") as u8;
    crate_setup
        .logging_setup()
        .with_log_panics(true)
        .with_verbosity(verbosity)
        .build()
        .expect("logging");

    let config_path = match matches.value_of("config") {
        Some(path) => Ok(PathBuf::from(path)),
        None => std::env::current_dir().map(|p| p.join("config.toml")),
    };
    let config = match config_path {
        Ok(config_path) => match config::load_config(&config_path) {
            Ok(config) => config,
            Err(e) => {
                anyhow::bail!("Error loading config: {}", e);
            }
        },
        Err(e) => {
            anyhow::bail!("Error loading config: {}", e);
        }
    };

    log::debug!("Loaded configuration: {:?}", config);

    log::info!("Starting GPTournament web v{}", version!());

    if let Some(path) = matches.value_of("model") {
        let path = Path::new(path);
        if !path.exists() {
            if let Err(e) = std::fs::create_dir_all(path) {
                anyhow::bail!("Failed to create path {:?} for model: {}", path, e);
            } else {
                log::info!("Created model path {:?}", path);
            }
        }
        log::info!("Model path: {:?}", path);
        gpt::set_model_path(path);
    } else {
        gpt::set_model_path_to_cwd();
    }
    log::debug!("Initializing data");

    log::trace!("Starting model load");
    let model = tokio::task::spawn(AsyncModel::new(config.model_type));
    log::trace!("Creating DB pool");
    let db_pool = r2d2::Pool::builder().build(diesel::r2d2::ConnectionManager::<
        diesel::SqliteConnection,
    >::new::<String>(
        config
            .db_path
            .as_ref()
            .map_or("./db.sqlite".into(), |path| path.clone()),
    ))?;
    log::trace!("Running DB setup");
    db::DbSetup::new().run_db_setup(&config, db::DbConn::get(&db_pool)?)?;
    log::trace!("Creating states");
    let games = state::Games::new();
    let generators = state::Generators::new();
    log::trace!("Building cookie setup");
    let cookies = api::helper::cookie::CookieBuilder::new(config.secret_key.clone())?;
    log::trace!("Waiting for model load");
    let model = Model::new(model.await??);
    log::debug!("Starting cleanup");
    let mut cleanup = resources::StateCleanup::new();
    cleanup.add_state(games.clone());

    log::debug!("Building web server");
    let local = tokio::task::LocalSet::new();
    let system = actix::System::run_in_tokio("actix-system", &local);
    let server = actix_web::HttpServer::new(move || {
        actix_web::App::new()
            .wrap(middleware::CookieMiddleware::new())
            .wrap(actix_middleware::Logger::default())
            .wrap(actix_middleware::NormalizePath::new(
                actix_middleware::normalize::TrailingSlash::Trim,
            ))
            .data(model.clone())
            .data(games.clone())
            .data(generators.clone())
            .data(config.clone())
            .data(db_pool.clone())
            .data(cookies.clone())
            .service(web::scope("/api").configure(api::config))
    })
    .bind("0.0.0.0:8000")?;
    log::info!("Launching server");
    let _ = server.run().await;
    let _ = local.await;
    let _ = system.await;

    Ok(())
}
